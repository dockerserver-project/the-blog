FROM node:lts-alpine as front

WORKDIR /app
COPY ./ /app/

RUN npm install
RUN npm run build

FROM nginx:1.17-alpine
RUN rm -rf /usr/share/nginx/html
COPY --from=front /app/public/ /usr/share/nginx/html
