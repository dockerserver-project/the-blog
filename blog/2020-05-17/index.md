---
date: '2020-05-17'
title: 'The DockerServer Project: The Goal'
banner: '/assets/bg/blue-drops.jpg'
tags: ['docker']
path: dockerserver-project-the-goal
---

The Goal in this project is to run a full automated web server using Docker containers in a single node, without the complexity of other cloud solutions like Kubernetes.
<!-- end -->
It will have the following features:

- Reverse proxy handling all https requests with SSL certificates auto creation
- Different types of sites running: Drupal, static pages, etc.
- Different types of database engines like MySQL, MariaDB, etc.
- Cron jobs to do maintenance tasks.
- CI/CD from GitLab using a custom runner.
- Daily backups to a cloud service like DropBox, Google Drive, etc.

I'm going to start with a fresh VPS server in which you need to [install Docker](https://docs.docker.com/engine/install/debian/) and [Docker Compose](https://docs.docker.com/compose/install/).

You'll also need a [GitLab account](https://gitlab.com).
