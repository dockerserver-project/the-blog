---
date: '2020-05-18'
title: 'Reverse proxy with Traefik'
banner: '/assets/bg/blue-drops.jpg'
tags: ['traefix', 'docker']
path: reverse-proxy-traefik
---

The first container we need to setup is [Traefik](https://traefik.io). It'll be the responsible to forward requests from the public Internet, the outside world, to our containers private network.

It'll also take care of SSL certificate generation with [Let's Encrypt](https://letsencrypt.org) so the public communications will be encrypted.
<!-- end -->
The following diagram illustrates the main idea:

```mermaid-svg
graph LR
subgraph Example request
A[User browser]
end
A -->|https| B[Traefik]
subgraph Internal network
B -->|http| C[Drupal]
end
```

Traefik will listen in the ports: 80 and 433 so that ports should be mapped from the container to our server.
