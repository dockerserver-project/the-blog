---
date: '2020-05-19'
title: 'Creating a blog with Gatsby and Nginx'
banner: '/assets/bg/blue-drops.jpg'
tags: ['gatsby', 'react', 'nginx', 'docker']
path: creating-blog-with-gatsby-nginx
---

In this article I’m going to create the static site for dockerserver.com and I’m going to use Gatsby for it. Then I’m going to create a deploy script that will pack all the static html files in a Nginx image.
<!-- end -->

## Creating the site

For this site what I want is a simple blog with a basic design. I'm going to use
one of the [Gatsby starters](node --inspect node_modules/.bin/gatsby develop) template as a base for my site.

You’ll need Node and NPM installed in your computer.

```bash
npm install -g gatsby-cli
gatsby new dockerserver.com https://github.com/mhadaily/gatsby-starter-typescript-power-blog
cd dockerserver.com
gatsby develop
```

Now you can open the address http://localhost:8000/ in your browser to see the site.

## Editing the site

To edit the site, open the project directory using your favorite IDE and start editing the file Gatsby-config.js. In this file you have all the site metadata and the plugins used by Gatsby. That metadata variables are used when creating the site home page. That home page is created by the file src/pages/index.tsx, this is the other file you’ll need to edit to setup your site

## Creating the Docker image

We need two Docker images to build ours.

First one is to run the Gatsby build. It inherits from the LTS node image and
basically copy all our source code and executes `npm run build`.

The second one inherits from the official Nginx image and add to it our build.

Let's go to create a `Dockerfile` in the project root:

```docker
FROM node:lts-alpine as front

WORKDIR /app
COPY ./ /app/

RUN npm install
RUN npm run build

FROM nginx:1.17-alpine
RUN rm -rf /usr/share/nginx/html
COPY --from=front /app/public/ /usr/share/nginx/html

```

We also need a `.dockerignore` file to avoid node_modules and some other cache directories to be included in our final build.

```
.cache/
node_modules/
public/
```

Then we can locally build our image just to test it:

```bash
docker build -t dockerserver/the-blog .
docker run --rm -p 8080:80 dockerserver/the-blog
```

Then point your browser to `http://localhost:8080/` and you should see your blog running.

## Deploying the site

We're going to use the GitLab `.gitlab-ci.yml` file to define the required steps to build and deploy our site.

```yaml
stages:
  - build
  - deploy

build:
  tags:
    - drupal
  stage: build
  variables:
    GIT_STRATEGY: fetch
  script:
    - docker build --tag $CI_REGISTRY_IMAGE .

deploy:
  tags:
    - drupal
  stage: deploy
  variables:
    GIT_STRATEGY: none
  script:
    - mkdir -p /srv/sites/$CI_PROJECT_NAME
    - cp dist/docker-compose.yml /srv/sites/$CI_PROJECT_NAME
    - docker-compose -f /srv/sites/$CI_PROJECT_NAME/docker-compose.yml up -d
  only:
    - master
```

The idea is to have each site in a different directory with a `docker-compose.yml` file to define and control it containers. The path I want to use is `/srv/sites/site-name`.

I'm going to create a `dist` folder to hold all files required to run the site in production, in this case just the `docker-compose-yml` file:

```yaml
version: '3.8'

networks:
  proxy:
    external:
      name: proxy_proxy

services:
  dockerserver:
    image: registry.gitlab.com/dockerserver-project/the-blog
    security_opt:
      - no-new-privileges:true
    networks:
      - proxy
    restart: always
    labels:
      - traefik.enable=true
      - traefik.http.routers.dockerserver.rule=Host(`www.dockerserver.com`) || Host(`dockerserver.com`)
      - traefik.http.routers.dockerserver.tls=true
      - traefik.http.routers.dockerserver.tls.certresolver=le
      - traefik.http.middlewares.dockerserver.compress=true
      - traefik.http.routers.dockerserver.middlewares=dockerserver@docker
```

Here I'm defining one service, the Gatsby generated site, and an external network. That network is the same network defined by the Traefik container.

The labels section defines all the settings Traefik needs to know in order to get the site available in the public Internet.

## ![GitLab pipeline](./gitlab-pipeline-ok.png)

Code repository for this site: https://gitlab.com/dockerserver-project/the-blog
