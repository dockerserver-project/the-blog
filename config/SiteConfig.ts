export default {
  pathPrefix: '/', // Prefix for all links. If you deploy your site to example.com/portfolio your pathPrefix should be "portfolio"

  siteTitle: 'The DockerServer Project', // Navigation and Site Title
  siteTitleAlt: 'A containerized auto deployable web server', // Alternative Site title for SEO
  siteUrl: 'https://www.dockerserver.com', // Domain of your site. No trailing slash!
  siteLanguage: 'en', // Language Tag on <html> element
  siteBanner: '/assets/banner.jpg', // Your image for og:image tag. You can find it in the /static folder
  defaultBg: '/assets/computer-desktop-cover.jpg', // default post background header
  favicon: 'static/assets/logo.png', // Your image for favicons. You can find it in the /src folder
  siteDescription: 'Typescript Power Blog with big typography', // Your site description
  author: 'Juan M. Sosso', // Author for schemaORGJSONLD
  bio: 'Drupal specialist and Docker enthusiast who lives in Málaga (Spain).',
  siteLogo: '/assets/logo.png', // Image for schemaORGJSONLD

  // siteFBAppID: '123456789', // Facebook App ID - Optional
  userTwitter: 'jmsosso', // Twitter Username - Optional
  ogSiteName: 'servinube', // Facebook Site Name - Optional
  ogLanguage: 'es_ES', // Facebook Language

  // Manifest and Progress color
  // See: https://developers.google.com/web/fundamentals/web-app-manifest/
  themeColor: '#3498DB',
  backgroundColor: '#2b2e3c',

  // Settings for typography.ts
  headerFontFamily: 'Bitter',
  bodyFontFamily: 'Open Sans',
  baseFontSize: '18px',

  // Social media
  siteFBAppID: '',

  //
  Google_Tag_Manager_ID: 'GTM-XXXXXXX',
  POST_PER_PAGE: 4,
};
