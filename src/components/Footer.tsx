import React from 'react';
import styled from 'styled-components';
import { StaticQuery, graphql } from 'gatsby';

const FooterWrapper = styled.footer`
  text-align: center;
  padding: 3rem 0;
  span {
    font-size: 0.75rem;
  }
`;

interface DataProps {
  site: {
    siteMetadata: {
      title: string;
    };
    buildTime: string;
  };
}

const Footer = () => {
  const query = graphql`
    {
      site {
        buildTime(formatString: "DD/MM/YYYY")
      }
    }
  `;
  const render = (data: DataProps) => {
    return (
      <FooterWrapper>
        DockerServer is a project from <a href="https://www.servinube.net">servinube</a> started in
        2020.
        <br />
        <span>Last build: {data.site.buildTime}</span>{' '}
      </FooterWrapper>
    );
  };

  return <StaticQuery query={query} render={render} />;
};

export { Footer };
