import React from 'react';
import { Link } from 'gatsby';
import config from '../../config/SiteConfig';

export default () => (
  <h1>
    <Link to="/">{config.siteTitle}</Link>
  </h1>
);
