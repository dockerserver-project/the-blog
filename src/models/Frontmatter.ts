interface Frontmatter {
  date: string;
  title: string;
  tags: string[];
  banner?: string;
  path: string;
}

export default Frontmatter;
