import React from 'react';
import { Content, Header, Layout, Wrapper } from '../components';
import { Helmet } from 'react-helmet';
import config from '../../config/SiteConfig';
import SiteTitle from '../components/SiteTitle';

export default () => {
  return (
    <Layout>
      <Wrapper>
        <Helmet title={`404 not found | ${config.siteTitle}`} />
        <Header>
          <SiteTitle />
        </Header>
        <Content>
          <h2>NOT FOUND</h2>
          <p>You just hit a route that doesn't exist...</p>
        </Content>
      </Wrapper>
    </Layout>
  );
};
