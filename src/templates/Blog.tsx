import React from 'react';
import { graphql } from 'gatsby';
import { Layout, Article, Wrapper, SectionTitle, Header, Content, Pagination } from '../components';
import { Helmet } from 'react-helmet';
import config from '../../config/SiteConfig';
import Data from '../models/Data';
import SiteTitle from '../components/SiteTitle';

interface Props {
  data: Data;
  pageContext: {
    currentPage: number;
    totalPages: number;
  };
}

export default (props: Props) => {
  const { currentPage, totalPages } = props.pageContext;

  const { data } = props;
  const { edges, totalCount } = data.allMarkdownRemark;

  return (
    <Layout>
      <Helmet title={`Blog | ${config.siteTitle}`} />
      <Header>
        <SiteTitle />
        <SectionTitle uppercase={true}>Latest stories</SectionTitle>
      </Header>
      <Wrapper>
        <Content>
          {edges.map((post) => (
            <Article
              title={post.node.frontmatter.title}
              date={post.node.frontmatter.date}
              excerpt={post.node.excerpt}
              timeToRead={post.node.timeToRead}
              slug={post.node.frontmatter.path}
              key={post.node.frontmatter.path}
            />
          ))}
          <Pagination currentPage={currentPage} totalPages={totalPages} url={'blog'} />
        </Content>
      </Wrapper>
    </Layout>
  );
};

export const BlogQuery = graphql`
  query($skip: Int!, $limit: Int!) {
    allMarkdownRemark(
      sort: { fields: [frontmatter___date], order: DESC }
      limit: $limit
      skip: $skip
    ) {
      totalCount
      edges {
        node {
          fields {
            slug
          }
          frontmatter {
            title
            date(formatString: "DD/MM/YYYY")
            path
          }
          excerpt(pruneLength: 500)
          timeToRead
        }
      }
    }
  }
`;
