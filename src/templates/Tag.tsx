import React from 'react';
import { Link } from 'gatsby';
import PageProps from '../models/PageProps';
import { Article, Content, Header, Layout, SectionTitle, Subline, Wrapper } from '../components';
import { Helmet } from 'react-helmet';
import config from '../../config/SiteConfig';
import SiteTitle from '../components/SiteTitle';

export default (props: PageProps) => {
  const { posts, tagName } = props.pathContext;
  const totalCount = posts ? posts.length : 0;
  const subline = `${totalCount} post${totalCount === 1 ? '' : 's'} tagged with "${tagName}"`;

  return (
    <Layout>
      <Helmet title={`${'Tags'} | ${config.siteTitle}`} />
      <Header>
        <SiteTitle />
        <SectionTitle>Tag: {tagName}</SectionTitle>
        <Subline sectionTitle={true}>
          {subline} (See <Link to="/tags">all tags</Link>)
        </Subline>
      </Header>
      <Wrapper>
        <Content>
          {posts
            ? posts.map((post: any, index) => (
                <Article
                  title={post.frontmatter.title}
                  date={post.frontmatter.date}
                  excerpt={post.excerpt}
                  slug={post.frontmatter.path}
                  timeToRead={post.timeToRead}
                  key={index}
                />
              ))
            : null}
        </Content>
      </Wrapper>
    </Layout>
  );
};
