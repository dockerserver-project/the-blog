const transitions = {
  normal: '0.5s',
};

const fontSize = {
  small: '0.9rem',
  big: '2.5rem',
  bigMobile: '1.8rem',
};

export { transitions, fontSize };
